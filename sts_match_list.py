import sts
import pymysql
import time


db = pymysql.connect("localhost","user","pass","db_name")
cursor = db.cursor()

while True:

    try:

        sts_handle = sts.STS()
        list_match = sts_handle.get_matches_urls()

        for url in list_match:
            query = "SELECT * FROM `matches` WHERE `url` = '%s';" % (url,)

            cursor.execute(query)
            reply = cursor.fetchall()

            if len(reply) == 0:

                match_info = sts.STSMatchInfo(url)

                bet_id = match_info.get_id_bet()
                options = match_info.get_options()
                date = match_info.get_date()
                date = "%s-%s-%s %s:%s:00" % (date[2], date[1], date[0], date[3], date[4],)
                courses = match_info.get_courses()

                query = "INSERT INTO `matches` (`id`, `id_match`, `url`, `date`, `option_1`, `option_2`) VALUES (NULL, '%s', '%s', '%s', '%s', '%s');" \
                        % (bet_id, url, date, options[0], options[1],)

                try:
                    cursor.execute(query)
                    db.commit()

                    query = "SELECT `id` FROM `matches` WHERE `url` = '%s';" % (url,)
                    cursor.execute(query)
                    reply = cursor.fetchall()

                    if len(reply) == 1:
                        query = "INSERT INTO `courses` (`id`, `id_match`, `cours_option_1`, `cours_option_2`, `time`) VALUES (NULL, '%s', '%s', '%s', CURRENT_TIMESTAMP);" \
                                % (reply[0][0], courses[0], courses[1], )
                        try:
                            cursor.execute(query)
                            db.commit()
                        except:
                            db.rollback()
                            print("Error :: add courses to new matche")

                except:
                    db.rollback()
                    print("Error :: add new matche to DB")

        print("-")
        sts_handle = None
        match_info = None

    except:
        print("Error :: get urls")

    time.sleep(30)




