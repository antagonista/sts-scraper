# Python STS Scraper

## Opis

Scraper pobiera informacje ze strony https://sts.pl/ i zapisuje je do bazy danych MySql. Póki co scraper obsługuje jedynie jedną kategorię sportową – Tenis. 

## Pliki

* sts.py - plik z głównymi klasami scrapera.
* sts.sql - struktura bazy danych MySql.
* sts_scraper_run.py - odpala wszystkie scrapery w tle (lepiej do CRON'a dodać zadania).
* sts_match_list.py - pobiera listę meczy, które można obstawiać w danej chwili.
* sts_check_courses.py - pobiera w sposób ciągły kurs danego meczu na dany moment (aby można było zaobserwować zmiany kursów w czasie względem konkretnych meczy).
* sts_check_scores.py - pobiera wyniki zakończonych meczy.
