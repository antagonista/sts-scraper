import sts
import pymysql
import time

db = pymysql.connect("localhost","user","pass","db_name")
cursor = db.cursor()

while True:

    try:
        scores = sts.STSscore()

        if scores.get_status() is True:

            for score_match in scores.get_array_info():

                query = "SELECT `id` FROM `matches` WHERE `option_1` LIKE '%s' AND `expired` = 1 AND `win_option` IS NULL AND `score` IS NULL" \
                        % ('%' + score_match[0] + '%',)
                cursor.execute(query)
                result = cursor.fetchall()

                if len(result) != 0:
                    bet_id = result[0][0]

                    len_score_array = len(score_match)
                    score = score_match[len_score_array-1]
                    win_option = score_match[len_score_array-2]

                    query = "UPDATE `matches` SET `win_option` = '%s', `score` = '%s' WHERE `matches`.`id` = %s;" \
                            % (win_option, score, bet_id,)

                    try:
                        cursor.execute(query)
                        db.commit()
                        print("Info :: score match updated")
                    except:
                        db.rollback()
                        print("Error :: match updated")
                        print(score_match[0])
                        print(score_match)
                        print("- - - - - - -")

    except:
        print('ERROR :: get url website (class init)')

    print('-')
    time.sleep(60)


