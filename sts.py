from bs4 import BeautifulSoup
import ssl
import re
from urllib.request import Request, urlopen

ssl._create_default_https_context = ssl._create_unverified_context

class STS:

    __url = 'https://www.sts.pl/pl/oferta/zaklady-bukmacherskie/'
    __tenis_urls = None
    __tenis_matches_urls = []

    def __init__(self):

        self.__get_website()
        self.__get_all_matches()

    def __get_website(self):

        req = Request(self.__url, headers={'User-Agent': 'Mozilla/5.0'})

        webpage = urlopen(req).read()

        soup = BeautifulSoup(webpage, features='html.parser')
        tenis_category = str(soup.find(id='sport_185'))

        self.__tenis_urls = []

        soup = BeautifulSoup(str(tenis_category), features='html.parser')
        for link in soup.findAll('a', attrs={'href': re.compile("^https://")}):
            self.__tenis_urls.append(link.get('href'))

    def __get_all_matches(self):

        __tenis_matches_urls = []

        for url in self.__tenis_urls:

            req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
            webpage = urlopen(req).read()

            soup = BeautifulSoup(webpage, features='html.parser')
            offer_table = str(soup.find(id='offerTables'))

            soup = BeautifulSoup(offer_table, features='html.parser')
            tbody_offer = soup.find_all('tbody')

            for table_match in tbody_offer:

                found_urls = []

                soup = BeautifulSoup(str(table_match), features='html.parser')
                for link in soup.findAll('a', attrs={'href': re.compile("^https://")}):

                    found_urls.append(link.get('href'))

                    self.__tenis_matches_urls.append(found_urls[0])

    def __unique_list(self, list):

        unique_list = []

        for x in list:
            if x not in unique_list:
                unique_list.append(x)

        return unique_list

    def get_matches_urls(self):
        return self.__unique_list(self.__tenis_matches_urls)


class STSMatchInfo:

    __url = None
    __html_webpage = None
    __expired = None # True = expired offer / False = actual offer

    __id_bet = None
    __date = None
    __options = None
    __options_courses = None

    def __init__(self, url):

        self.__url = url
        self.__scrap_bet_id()
        self.__load_website()
        self.__offer_status()


    def __load_website(self):

        req = Request(self.__url, headers={'User-Agent': 'Mozilla/5.0'})
        self.__html_webpage = urlopen(req).read()

    def __offer_status(self):
        soup = BeautifulSoup(self.__html_webpage, features='html.parser')
        result = soup.find(id='no_matches')

        if result is None:
            self.__expired = False
        else:
            self.__expired = True

    def __scrap_bet_id(self):

        id_bet = re.findall(r'oppty=([0-9]+)', self.__url)
        self.__id_bet = id_bet[0]

    def __scrap_date(self):

        soup = BeautifulSoup(self.__html_webpage, features='html.parser')
        date = soup.findAll("div", {"class": "h2hstats_date"})

        date = re.findall(r'<div class="h2hstats_date">(.*)</div>', str(date[0]))

        date = date[0]
        date = date.split()
        date = date[1]

        date = re.findall(r'([0-9]+).([0-9]+).([0-9]+)-([0-9]+):([0-9]+)', date)
        date = date[0]

        self.__date = date

    def __scrap_options_and_courses(self):

        self.__options = []
        self.__options_courses = []

        soup = BeautifulSoup(self.__html_webpage, features='html.parser')
        main_table = soup.findAll("div", {"class": "shadow_box support_bets_offer"})

        soup = BeautifulSoup(str(main_table), features='html.parser')
        main_table = soup.findAll("table", {"class": "col2"})

        soup = BeautifulSoup(str(main_table[0]), features='html.parser')
        td_bets = soup.findAll("td", {"class": "bet"})

        for td_bet in td_bets:
            soup = BeautifulSoup(str(td_bet), features='html.parser')
            link = soup.find('a')

            found_text = link.findAll(text=True)

            found_course = found_text[1]
            found_option = found_text[0]
            found_option = found_option.replace("\n", "")

            self.__options.append(found_option)
            self.__options_courses.append(found_course)

    def get_expired_status(self):
        return self.__expired

    def get_id_bet(self):
        return self.__id_bet

    def get_date(self):

        if self.__expired is False:
            self.__scrap_date()
            return self.__date
        else:
            return False

    def get_options(self):

        if self.__expired is False:

            if self.__options is None:
                self.__scrap_options_and_courses()

            return self.__options

        else:
            return False

    def get_courses(self):

        if self.__expired is False:

            if self.__options_courses is None:
                self.__scrap_options_and_courses()

            return self.__options_courses

        else:
            return False


class STSscore:

    __succes = None

    __url = "https://www.sts.pl/pl/oferta/zaklady-bukmacherskie/wyniki/?&sport=185"
    __html_webpage = None

    __array_info = None

    def __init__(self):
        self.__array_info = []

        self.__load_webpage()
        self.__scrab_matches()

    def __load_webpage(self):
        req = Request(self.__url, headers={'User-Agent': 'Mozilla/5.0'})
        self.__html_webpage = urlopen(req).read()

    def __scrab_matches(self):
        soup = BeautifulSoup(self.__html_webpage, features='html.parser')
        result = soup.find(id='results')

        if result is not None:
            #class=prilezitost

            soup = BeautifulSoup(str(result), features='html.parser')
            matches = soup.findAll("tr", {"class": "prilezitost"})

            if matches is not None:

                for match in matches:

                    soup = BeautifulSoup(str(match), features='html.parser')
                    options = soup.find('th', {"class": "zapas"})
                    options = options.findAll(text=True)

                    options = options[0].replace('\n','').split(' - ')

                    scores = soup.find_all('td', {"class": "center"})

                    score_type = scores[0].findAll(text=True)
                    score = scores[1].findAll(text=True)
                    score_type = score_type[0]
                    score = score[0]

                    if score_type == 'zwrot':
                        score = '-'
                        score_type = 0

                    array_info = []
                    array_info.append(options[0])
                    array_info.append(options[1])
                    array_info.append(score_type)
                    array_info.append(score)

                    self.__array_info.append(array_info)

                    self.__succes = True

            else:
                self.__succes = False

        else:
            self.__succes = False

    def get_status(self):
        return self.__succes

    def get_array_info(self):
        return self.__array_info

"""
Sts = STS()

list = Sts.get_matches_urls()
print(list[0])

match_info = STSMatchInfo(list[0])
print(match_info.get_id_bet())
print(match_info.get_match_date())
print(match_info.get_options())
print(match_info.get_courses())


test = STSMatchInfo("https://www.sts.pl/pl/oferta/zaklady-bukmacherskie/zaklady-sportowe/?action=offer&sport=185&region=6608&league=77112&oppty=191575312")
print(test.get_expired_status())
"""

