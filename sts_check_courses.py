import sts
import pymysql
import time

db = pymysql.connect("localhost","user","pass","db_name")
cursor = db.cursor()

while True:
    try:

        query = "SELECT `id`, `url` FROM `matches` WHERE `expired` IS NULL"
        cursor.execute(query)
        reply = cursor.fetchall()

        for match in reply:
            try:

                match_info = sts.STSMatchInfo(match[1])
                expired = match_info.get_expired_status()

                if expired is True:
                    # Offer expired
                    query = "UPDATE `matches` SET `expired` = '1' WHERE `matches`.`id` = '%s';" % (match[0],)

                    try:
                        cursor.execute(query)
                        db.commit()
                        print("Info :: offer status changed (expired)")
                    except:
                        db.rollback()
                        print("Error :: change status offer")

                else:

                    courses = match_info.get_courses()

                    query = "INSERT INTO `courses` (`id`, `id_match`, `cours_option_1`, `cours_option_2`, `time`) VALUES (NULL, '%s', '%s', '%s', CURRENT_TIMESTAMP);" \
                            % (match[0], courses[0], courses[1], )

                    try:
                        cursor.execute(query)
                        db.commit()

                    except:
                        db.rollback()
                        print("ERROR :: dont add fresh courses")

            except:
                print("ERROR 2")
                print(match[1])

    except:
        print("ERROR")

    print("-")
    time.sleep(5)

